const ip = require('ip')

const {Kafka, logLevel} = require('kafkajs')

const host = process.env.HOST_IP || ip.address()
const tradesManager = require('../Logic/tradeManager');
const fs = require("fs");
const path = require("path");


const kafka = new Kafka({
    logLevel: logLevel.INFO,
    brokers: [``],
    clientId: 'example-consumer',
    sasl: { mechanism: '', password:"", username: "" },

    ssl: {
        rejectUnauthorized: false,
        ca: '',
        cert: '',
        key: '',

    }
})

const topic = ''
const consumer = kafka.consumer({groupId: ''})

async function run() {
    await consumer.connect()
    await consumer.subscribe({topic, fromBeginning: true})
    await consumer.run({
        // eachBatch: async ({ batch }) => {
        //   console.log(batch)
        // },
        eachMessage: async ({topic, partition, message}) => {
            // console.log(message.value.toString())
            await tradesManager.generateData(message.value.toString(), message.key.toString());
        },
    })
}

run().catch(e => console.error(`[example/consumer] ${e.message}`, e))

const errorTypes = ['unhandledRejection', 'uncaughtException']
const signalTraps = ['SIGTERM', 'SIGINT', 'SIGUSR2']

errorTypes.forEach(type => {
    process.on(type, async e => {
        try {
            console.log(`process.on ${type}`)
            console.error(e)
            await consumer.disconnect()
            process.exit(0)
        } catch (_) {
            process.exit(1)
        }
    })
})

signalTraps.forEach(type => {
    process.once(type, async () => {
        try {
            await consumer.disconnect()
        } finally {
            process.kill(process.pid, type)
        }
    })
})
run();