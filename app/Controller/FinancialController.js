const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const port = 2020;

const ohlcManager = require('../Logic/financialManager');

app.get('/financial', async function (req, res) {

    const pairId = req.query.pair_id;

    if (req.query.pair_id) {
            const data =  await ohlcManager.getFinancialByPairId((Number)(pairId));

        res.json(data)

    }else{
            const data = await ohlcManager.getFinancial();

        res.json(data)

    }



})
app.listen(port, function () {
    console.log(`server started on port ${port}`)
});