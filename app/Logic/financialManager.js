const tradesModel = require('../Models/trades');
const pairManager = require('../Logic/pairManager');
const {

} = require('sequelize');
// const Path = require("path");
const redis = require("redis");

const client = redis.createClient();

async function connectToRedis() {
    await client.connect();
}

async function calculateFinancial2() {
    const now = Math.floor(new Date().getTime() / 1000);
    const yesterday = Math.floor(new Date().getTime() / 1000) - 86400;

    let markets = await pairManager.getPairs();
    markets = JSON.parse(markets);
    let modifiedmarket = await Promise.all(markets.map(async function (element) {
        const open = await tradesModel.findOHLC(element, 'unixTime', 'ASC', now, yesterday);
        const close = await tradesModel.findOHLC(element, 'unixTime', 'DESC', now, yesterday);
        const high = await tradesModel.findOHLC(element, 'price', 'DESC', now, yesterday);
        const low = await tradesModel.findOHLC(element, 'price', 'ASC', now, yesterday);
        const baseVolume = await tradesModel.calculateVolume(element, 'amount2', now, yesterday);
        const qouteVolume = await tradesModel.calculateVolume(element, 'amount1', now, yesterday);

        await client.set('pair:' + element.toString() + '-base-volume', JSON.stringify(baseVolume));
        await client.set('pair:' + element.toString() + '-ohlc_close', JSON.stringify(close));
        await client.set('pair:' + element.toString() + '-ohlc_high', JSON.stringify(high));
        await client.set('pair:' + element.toString() + '-ohlc_low', JSON.stringify(low));
        await client.set('pair:' + element.toString() + '-ohlc_open', JSON.stringify(open));
        await client.set('pair:' + element.toString() + '-qoute-volume', JSON.stringify(qouteVolume));

    }));


}

async function calculateFinancial(pair) {

    let list = [];
    let quoteVolume = 0;
    let baseVolume = 0;

    let i = 0;
    const now = Math.floor(new Date().getTime() / 1000);
    const yesterday = Math.floor(new Date().getTime() / 1000) - 99000;

    let pairDetails = await tradesModel.getData(pair, now, yesterday);

    let calculate = await Promise.all(pairDetails.map(async function (pairDetail) {
        list[i] = Number(pairDetail.price)
        i++;
    }));

    let modifiedmarket2 = await Promise.all(pairDetails.map(async function (element) {
        quoteVolume = (Number)(element.amount1) + quoteVolume
        baseVolume = (Number)(element.amount2) + baseVolume

    }));

    //baseVolume
    if (baseVolume === null) {
        baseVolume = 0;
    }
    await client.set('pair' + pair.toString() + '-base-volume', JSON.stringify(baseVolume));


    //quoteVolume
    if (quoteVolume === null) {
        quoteVolume = 0
    }
    await client.set('pair' + pair.toString() + '-qoute-volume', JSON.stringify(quoteVolume));

    //close:
    let close = list[0];
    if (typeof close == "undefined") {
        close = 0;
    }
    await client.set('pair' + pair.toString() + '-ohlc_close', JSON.stringify(close));

    //open:
    let open = list[list.length - 1];
    if (typeof open == "undefined") {
        open = 0;
    }
    await client.set('pair' + pair.toString() + '-ohlc_open', open.toString());

    //high
    let max = Math.max(...list);
    if (typeof max == "undefined") {
        max = 0;
    }
    await client.set('pair' + pair.toString() + '-ohlc_high', max.toString());

    //low
    let min = Math.min(...list);
    if (typeof min == "undefined") {
        min = 0;
    }
    await client.set('pair' + pair.toString() + '-ohlc_low', min.toString());

}

async function getFinancial2(pairId) {
    let data = {};
    let markets = await pairManager.getPairs();
    const pairIdExists = markets.includes(pairId);
    if (!pairIdExists) {
        data = {
            message: 'selected pairId is not defined.'
        }

    } else {

        const precision = await pairManager.getPersision(pairId);
        const basePersision = Math.pow(10, -JSON.parse(precision.base_precision));
        const quotePersison = Math.pow(10, -JSON.parse(precision.quote_precision));
        const baseVolume = await client.get('pair' + pairId.toString() + '-base-volume');
        const qouteVolume = await client.get('pair' + pairId.toString() + '-qoute-volume');
        const open = await client.get('pair' + pairId.toString() + '-ohlc_open');
        const close = await client.get('pair' + pairId.toString() + '-ohlc_close');
        const high = await client.get('pair' + pairId.toString() + '-ohlc_high');
        const low = await client.get('pair' + pairId.toString() + '-ohlc_low');

        const changePercent = (((Number)(close) - (Number)(open)) * 10000 / (Number)(open)) / 100
        data = {
            base_volume: (Number)(((JSON.parse(baseVolume)) / basePersision).toFixed(2)),
            change_percent: (Number)(changePercent.toFixed(2)),
            close: (Number)(close),
            highest: (Number)(high),
            lowest: (Number)(low),
            open: (Number)(open),
            quote_volume: (Number)(Math.floor((JSON.parse(qouteVolume)) * quotePersison).toFixed(2)),

        }
    }

    return data;

}

async function getFinancial() {
    let data = [];
    let i = 0;
    let markets = await pairManager.getPairs();

    let modifiedmarket = await Promise.all(markets.map(async function (pairId) {
        const precision = await pairManager.getPersision(pairId);
        const basePersision = Math.pow(10, -JSON.parse(precision.base_precision));
        const quotePersison = Math.pow(10, -JSON.parse(precision.quote_precision));

        const baseVolume = await client.get('pair' + pairId.toString() + '-base-volume');
        const qouteVolume = await client.get('pair' + pairId.toString() + '-qoute-volume');
        const open = await client.get('pair' + pairId.toString() + '-ohlc_open');
        const close = await client.get('pair' + pairId.toString() + '-ohlc_close');
        const high = await client.get('pair' + pairId.toString() + '-ohlc_high');
        const low = await client.get('pair' + pairId.toString() + '-ohlc_low');

        const changePercent = (((Number)(close) - (Number)(open)) * 10000 / (Number)(open)) / 100
        data[i] = {
            pair_id: pairId,
            base_volume: (Number)(((JSON.parse(baseVolume)) / basePersision).toFixed(2)),
            change_percent: (Number)(changePercent.toFixed(2)),
            close: (Number)(close),
            highest: (Number)(high),
            lowest: (Number)(low),
            open: (Number)(open),
            quote_volume: (Number)(Math.floor((JSON.parse(qouteVolume)) * quotePersison).toFixed(2)),

        }
        i++;
    }));

    return data;
}

async function getFinancialByPairId(pairId) {
    let data;
    const now = Math.floor(new Date().getTime() / 1000);
    const yesterday = Math.floor(new Date().getTime() / 1000) - 99000;
    const precision = await pairManager.getPersision(pairId);
    const basePersision = Math.pow(10, -JSON.parse(precision.base_precision));
    const quotePersison = Math.pow(10, -JSON.parse(precision.quote_precision));
    const pricePrecision = JSON.parse(precision.price_precision);

    const baseVolume = await client.get('pair' + pairId.toString() + '-base-volume');
    const qouteVolume = await client.get('pair' + pairId.toString() + '-qoute-volume');
    const open = await client.get('pair' + pairId.toString() + '-ohlc_open');
    const close = await client.get('pair' + pairId.toString() + '-ohlc_close');
    const high = await client.get('pair' + pairId.toString() + '-ohlc_high');
    const low = await client.get('pair' + pairId.toString() + '-ohlc_low');

    const changePercent = (((Number)(close) - (Number)(open)) * 10000 / (Number)(open)) / 100
    data = {
        pair_id: pairId,
        base_volume: (Number)(((JSON.parse(baseVolume)) / basePersision).toFixed(2)),
        change_percent: (Number)(changePercent.toFixed(2)),
        close: (Number)(close).toFixed(pricePrecision),
        highest: (Number)(high).toFixed(pricePrecision),
        lowest: (Number)(low).toFixed(pricePrecision),
        open: (Number)(open).toFixed(pricePrecision),
        quote_volume: (Number)(Math.floor((JSON.parse(qouteVolume)) * quotePersison).toFixed(2)),
        count: await tradesModel.getCount(pairId, now, yesterday)
    }
    return data;
}

function convertTOString(obj) {
    const editedData = obj.toString().replace("[", " ");
    const finalData = editedData.toString().replace("]", " ");
    return JSON.parse(finalData).price;
}

connectToRedis()

module.exports = {
    getFinancial,
    calculateFinancial,
    getFinancialByPairId
}