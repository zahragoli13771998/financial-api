// const fetch = require("node-fetch");
const pairModel = require('../Models/pair');
const redis = require("redis");

const client = redis.createClient();

async function connectToRedis() {
    await client.connect();
}

async function setPersision() {

    const pairs = await pairModel.getDetail();

    let modifiedmarket = await Promise.all(pairs.map(async function (pair) {
        await client.set('base_persision_' + JSON.stringify(pair.pair_id), JSON.stringify(pair.base_precision));
        await client.set('quote_precision_' + JSON.stringify(pair.pair_id), JSON.stringify(pair.quote_precision));
        await client.set('price_precision' + JSON.stringify(pair.pair_id), JSON.stringify(pair.price_precision));

    }));

}

async function getPersision(pair_id) {
    let quote_precision;
    let base_precision;
    let price_precision;
    base_precision = await client.get('base_persision_' + pair_id.toString());
    quote_precision = await client.get('quote_precision_' + pair_id.toString());
    price_precision = await client.get('price_precision' + pair_id.toString());
    if (base_precision === null || quote_precision === null) {
        await setPersision();
        await getPairs();
        base_precision = await client.get('base_persision_' + pair_id.toString());
        quote_precision = await client.get('quote_precision_' + pair_id.toString());
        price_precision = await client.get('price_precision' + pair_id.toString());

        data = {
            base_precision: base_precision,
            quote_precision: quote_precision,
            price_precision: price_precision,
        }
        return data;

    } else {
        data = {
            base_precision: base_precision,
            quote_precision: quote_precision,
            price_precision: price_precision,
        }

        return data;
    }

}


async function getPairs() {
    let pairsList = [];
    const pairListKey = 'pairsList';
    let cachePairsList = await client.get(pairListKey)

    if (cachePairsList === null) {
        const Pairs = await pairModel.getPairs();

        Pairs.forEach(function (pair) {
            pairsList.push(pair.pair_id);
        });
        await client.set(pairListKey, JSON.stringify(pairsList));

        return pairsList;
    } else {
        return JSON.parse(cachePairsList);
    }
}

connectToRedis();
module.exports = {getPairs, getPersision}