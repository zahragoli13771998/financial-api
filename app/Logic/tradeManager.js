const redis = require('redis');
const pairManager = require('../Logic/pairManager');
const tradesModel = require('../Models/trades');
const {md5} = require("request/lib/helpers");
const {Logger} = require("sequelize/lib/utils/logger");

const client = redis.createClient();

async function connect() {
    await client.connect();
}

async function generateData(value, key) {
    const title = key.toString()
    let price;

    if (title.includes('buy_sell_')) {
        if (JSON.parse(value).amount1 === 0 || JSON.parse(value).amount2 === 0) {
            return 1;
        } else {
            let pairI = JSON.parse(value).pair_id;
            const precision = await pairManager.getPersision(pairI);

            const tradeTime = (Number)(title.substr(1, 10));
            const transactionId = title.replace('_buy_sell_', '').toString();
            const transactionIdMd5 = md5(transactionId + 'safda')

            price = (JSON.parse(value).amount1 * (Math.pow(10, -precision.quote_precision))) / ((JSON.parse(value).amount2 * (Math.pow(10, -precision.base_precision))));

            const amout2withPersision = JSON.parse(value).amount2 * (Math.pow(10, -precision.base_precision - 2));

            const data = {
                currencyId1: JSON.parse(value).currency_id1,
                currencyId2: JSON.parse(value).currency_id2,
                price: price.toFixed(5),
                pairId: JSON.parse(value).pair_id,
                amount1: JSON.parse(value).amount1,
                amount2: JSON.parse(value).amount2,
                type: JSON.parse(value).type,
                unixTime: JSON.parse(value).created_at,
                transactionIdMd5: transactionIdMd5,
            }
            await tradesModel.create(data);

            return data;
        }
    }


}


async function setTradesHistory() {

    const client = redis.createClient();
    await client.connect();
    let markets = await pairManager.getPairs();

    for (var market of markets) {
        const transactions = await tradesModel.getLastTrades((Number)(market));
        let list = [];

        for (var transaction of transactions) {

            const unix = (Number)(transaction.unixTime)
            var editedUnix = new Date(unix * 1e3).toISOString()
            var unixToTimeStamp = editedUnix.toString().replace('T', " ");
            var timeStamp = unixToTimeStamp.toString().split(".");
            const md5 = (transaction.transactionIdMd5.toString().substr(0, 32));

            const data = [(Number)(transaction.price), transaction.amount2, timeStamp[0], transaction.type, transaction.unixTime, md5, transaction.pairId]

            list.push(data)
        }
        await client.set('trades_history_of:' + market, JSON.stringify(list));

    }
}

async function getTradesHistory(pairId) {

    const history = await client.get('trades_history_of:' + pairId);
    return JSON.parse(history);
}


connect();
module.exports = {
    getTradesHistory,
    setTradesHistory,
    generateData

}