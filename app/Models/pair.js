const {Sequelize, DataTypes, Model} = require('sequelize');
const sequelize = new Sequelize('', {
    define: {
        timestamps: false
    }
})

class pair extends Model {
}

pair.init({
        base_ENname: {
            type: DataTypes.STRING
        },
        base_FAname: {
            type: DataTypes.STRING

        },
        quote_ENname: {
            type: DataTypes.STRING

        },
        quote_FAname: {
            type: DataTypes.STRING

        },
        base_currencyId: {
            type: DataTypes.INTEGER
        },
        quote_currencyId: {
            type: DataTypes.INTEGER
        },

        pair_id: {
            type: DataTypes.INTEGER
        },

        base_precision: {
            type: DataTypes.INTEGER
        },
        quote_precision: {
            type: DataTypes.INTEGER
        },
        price_precision: {
            type: DataTypes.INTEGER
        },
    }
    , {sequelize});



async function getPairs() {
    return pair.findAll({
        attributes: ['pair_id'],

    });
}

async function getDetail() {

    return pair.findAll({
        attributes: ['base_precision', 'quote_precision','pair_id','price_precision'],

    });
}

function deletePairDetail() {
    pair.destroy({
        truncate: true
    });
}

module.exports = {getPairs,getDetail};