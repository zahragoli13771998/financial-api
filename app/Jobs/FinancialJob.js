const schedule = require('node-schedule');
const financialManager = require('../Logic/financialManager');
const pairManager = require("../Logic/pairManager");
const {log} = require("debug");

async function runSchedule() {

    const job = schedule.scheduleJob('*/5 * * * * *', async function () {
        try {
            const pairs = await pairManager.getPairs();
            let modifiedmarket = await Promise.all(pairs.map(async function (pair) {

                await financialManager.calculateFinancial(pair);
            }));

        } catch (e) {
            console.log(e)
        }
    });
}

runSchedule();

module.exports = {
    runSchedule
}